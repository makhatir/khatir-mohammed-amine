const UserSession = require('./../models/UserSession');
var mongoose = require('mongoose'), 
    usersession = mongoose.model('UserSession');

exports.list_all_session = function (req, res) {
  UserSession.find({}, function (err, usersession) {
    if (err) res.send(err)
    res.json(usersession)
  })
}
 

exports.read_a_profile = function (req, res) {
  UserSession.findById(req.params._id, function (err, usersession) {
    if (err) res.send(err)
    res.json(usersession)
  }) 
}
exports.update_a_profile = function (req, res) {
 UserSession.findByIdAndUpdate(req.params.userId, req.body, function (err, usersession) {
    if (err) return next(err);
    res.json(usersession);
  }) 
} 