const Qcm = require('./../models/Qcm');
var mongoose = require('mongoose'), qcm = mongoose.model('Qcms');

 
exports.list_all_qcms = function (req, res) {
  Qcm.find({}, function (err, qcm) {
    if (err) res.send(err)
    res.json(qcm)
  })
}

exports.create_a_qcm = function (req, res) {
  var new_qcm = new Qcm(req.body)
  new_qcm.save(function (err, qcm) {
    if (err) res.send(err)
    res.json(qcm)
  })  
}  

exports.read_a_qcm = function (req, res) {
  Qcm.findById(req.params._id, function (err, qcm) {
    if (err) res.send(err)
    res.json(qcm)
  }) 
} 

exports.update_a_qcm= function (req, res) {
 Qcm.findByIdAndUpdate(req.params._id, req.body, function (err, qcm) {
    if (err) return next(err);
    res.json(qcm); 
  }) 
} 
 

exports.delete_a_qcm = function (req, res) {
  Qcm.remove(
    {
      _id: req.params._id  
    },
    function (err, qcm) {
      if (err) res.send(err)
      res.json({ message: 'Qcm successfully deleted' })
    }
  )
}
 