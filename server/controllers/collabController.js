const Collab = require('./../models/Collab');
var mongoose = require('mongoose'), collab = mongoose.model('Categories');
 
exports.list_all_categories = function (req, res) {
  Categorie.find({}, function (err, categorie) {
    if (err) res.send(err)
    res.json(categorie)
  })
}

exports.create_a_categorie = function (req, res) {
  var new_categorie = new Categorie(req.body)
  new_categorie.save(function (err, categorie) {
    if (err) res.send(err)
    res.json(categorie)
  })
}

exports.read_a_categorie = function (req, res) {
  Categorie.findById(req.params._id, function (err, categorie) {
    if (err) res.send(err)
    res.json(categorie)
  }) 
}

exports.update_a_categorie= function (req, res) {
 Categorie.findByIdAndUpdate(req.params._id, req.body, function (err, categorie) {
    if (err) return next(err);
    res.json(categorie);
  }) 
} 
 

exports.delete_a_categorie = function (req, res) {
  Categorie.remove(
    {
      _id: req.params._id 
    },
    function (err, categorie) {
      if (err) res.send(err)
      res.json({ message: 'User successfully deleted' })
    }
  )
}

