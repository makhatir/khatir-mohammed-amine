const mongoose = require('mongoose');

 
const CollabSchema= new mongoose.Schema({
  nom: {
    type: String,
    default: ''
  },
  prenom: {
      type: String,
      default:''
  }, 
  email: {
    type: String,
    default:''
},
  
  agence: {
    type: String,
    default: ''
  },
   
  isDeleted: {
    type: Boolean,
    default: false
  }
});
module.exports = mongoose.model('Collab', CollabSchema);
