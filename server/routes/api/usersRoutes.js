const users = require('../../models/User');
const mongoose = require('mongoose');

module.exports = (app) => {
  var users = require('../../controllers/usersController');
  var usersession = require('../../controllers/sessionController');
 

  // Users routes
  app.route('/api/account/users')
    .get(users.list_all_users)
    .post(users.create_a_user);
     

  app
    .route('/api/account/users/:userId')
    .get(users.read_a_user)
    .put(users.update_a_user)
    .delete(users.delete_a_user);
    
     app.route('/api/account/usersession')
    .get(usersession.list_all_session);
    
    
   app
    .route('/api/account/usersession/:_id')
    .get(usersession.read_a_profile)
    .put(usersession.update_a_profile);
     
 
};
