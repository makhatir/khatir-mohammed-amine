const Collab= require('../../models/Collab');
const mongoose = require('mongoose');
module.exports = (app) => {
  // -----------------api post signup---------------------------
    
    app.post('/api/account/signup', (req, res, next) => {
        const { body } = req;
       
        const {
            nom,
            prenom,
            agence
        } = body;
        let {
            email
        } = body;
        console.log('body', body);
        if(!nom) {
            return res.send({
                success: false,
                message: 'Error: First name cannot be blank.'
            }); 
        }
        if(!prenom) {
            return res.send({
                success: false,
                message: 'Error: Last name cannot be blank.'
            }); 
         }
         if(!email) {
            return res.send({
                success: false,
                message: 'Error: Email cannot be blank.'
            }); 
         }
         if(!agence) {
            return res.send({
                success: false,
                message: 'Error: agence cannot be blank.'
            }); 
         }

         console.log('here');

        email = email.toLowerCase();

         // Steps:
         // 1. Verify email dosen't exist
         // 2. Save
         Collab.find({
             email: email
         }, (err, previousUsers) => {
            if(err) {
                return res.send({
                success: false,
                message: 'Error: Server error'
            }); 
            } else if (previousUsers.length > 0) {
                return res.send({
                success: false,
                message: 'Error: Account already exist.'
            }); 
            }
           
          
            // Save the new user
            const newCollab = new Collab();
            newCollab.userId = newCollab._id;
            newCollab.nom = nom;
            newCollab.email = email;
            newCollab.prenom = prenom;
            newCollab.agence = agence;
            newCollab.save((err, collab) => {
                if (err) {
                    return res.send({
                        success: false,
                        message: 'Error: Server error'
                    }); 
                }
                return res.send({
                    success: true,
                    message: 'Your Sign Up Is Successful',
                     token: collab._id,
                    
                }); 
            });
         });
       
        
            });
    
    //------------------------------------
  
 
    
    
 
 
    

};
