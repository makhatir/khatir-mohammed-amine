const User = require('../../models/User');
const mongoose = require('mongoose');
module.exports = (app) => {
  // -----------------api post signup---------------------------
    
    app.post('/api/account/signups', (req, res, next) => {
        const { body } = req;
       
        const {
            nom,
            prenom,
            password
        } = body;
        let {
            email
        } = body;
        console.log('body', body);
        if(!nom) {
            return res.send({
                success: false,
                message: 'Error: First name cannot be blank.'
            }); 
        }
        if(!prenom) {
            return res.send({
                success: false,
                message: 'Error: Last name cannot be blank.'
            }); 
         }
         if(!email) {
            return res.send({
                success: false,
                message: 'Error: Email cannot be blank.'
            }); 
         }
         if(!password) {
            return res.send({
                success: false,
                message: 'Error: Password cannot be blank.'
            }); 
         }

         console.log('here');

        email = email.toLowerCase();

         // Steps:
         // 1. Verify email dosen't exist
         // 2. Save
         User.find({
             email: email
         }, (err, previousUsers) => {
            if(err) {
                return res.send({
                success: false,
                message: 'Error: Server error'
            }); 
            } else if (previousUsers.length > 0) {
                return res.send({
                success: false,
                message: 'Error: Account already exist.'
            }); 
            }
           
          
            // Save the new user
            const newUser = new User();
            newUser.userId = newUser._id;
            newUser.nom = nom;
            newUser.email = email;
            newUser.prenom = prenom;
            newUser.password = newUser.generateHash(password);
            newUser.save((err, user) => {
                if (err) {
                    return res.send({
                        success: false,
                        message: 'Error: Server error'
                    }); 
                }
                return res.send({
                    success: true,
                    message: 'Your Sign Up Is Successful',
                     token: user._id,
                    
                }); 
            });
         });
       
        
            });
    
    //------------------------------------
  
 
    
    
 
 
    

};
