
import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import Auth from '../Auth/Auth';
import {
  Badge,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Dropdown
} from 'reactstrap';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'
import axios from 'axios';
import 'whatwg-fetch';
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
class HeaderDropdown extends Auth {

  constructor(props) {
    super(props);
   

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false,
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }
   
  dropAccnt() {
    return (
      <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle nav>
          <img src={'http://icons.iconarchive.com/icons/icons8/ios7/512/Users-User-Male-2-icon.png'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
        </DropdownToggle>
        
        <DropdownMenu right>
         
          <DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
          <DropdownItem onClick={this.logout}><i className="fa fa-lock" ></i> Logout</DropdownItem>    
        </DropdownMenu>
      </Dropdown>
    );
  }

  render() {
    const {attributes  } = this.props;
   
    return (
      this.dropAccnt()
    );
  }
}

export default HeaderDropdown;
