
import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
 
import {Bar, Line} from 'react-chartjs-2';
 import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Jumbotron, Button, Container,
  Badge,
  Progress,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  CardTitle,
  ButtonToolbar,
  ButtonGroup,
  ButtonDropdown,
  Label,
  Input,
  Table
} from 'reactstrap';

import { Player } from 'video-react';


import axios from 'axios';
 
 
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';

class Profile extends Component {
 
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      token: '',
    qcm:[],
    usersession: {} ,
    };


  }

  componentDidMount() {
    
    const obj = getFromStorage('the_main_app');
      if(obj && obj.token){
            const {token} = obj;
           fetch('/api/account/signin')
           .then(res => res.json()) 
      }
  
    if (obj && obj.token) {
      const {token} = obj;
          
      //Verify token
      fetch('/api/account/verify?token=' + token)
    
      .then(res => res.json())
      .then(json => {
        if (json.success) {
          this.setState({
            token,
            isLoading: false,
          });
          axios.get('/api/account/usersession/'+ token)
          .then(res => {
            
            this.setState({ usersession: res.data });
            
           
          });
       
        } else {
          this.setState({
            isLoading: false,
          });
       
        }
      });
    } else {
      this.setState({
        isLoading: false,
      });
    }
    
      
  }
    

  

 
  ListnewQcm (){
 axios.get('/api/account/qcm')
      .then(res => {
        this.setState({ qcms: res.data });
       
      });

    if(!this.state.qcms) return;
  
    return (<div>
       <Row id="allqcm">
           {this.state.qcms.map(qcm =>
                 <Col xs="12" sm="6" lg="3">
                 <Card id="cardspli">
              <CardHeader> <h3 className="text-center" > <Link to={`/showQcm/${qcm._id}`} >{qcm.titre}</Link></h3>  </CardHeader>
              <CardBody>
 
                    <h4 className="text-center" >{qcm.description}</h4>
                    <h5 className="text-center">  <i className="fa fa-clock-o fa-lg mt-4"> {qcm.duree}  Min</i> </h5>
                     <h6 className="text-center"> Level is {qcm.level}</h6>
                    
                   
              
  </CardBody>
     <CardFooter>
                
                 <div className="text-center">  <strong > Created  By {qcm.auteur} | {qcm.categorie}</strong> </div>
           
              </CardFooter>
            </Card>
                    </Col>
                )}
        
 
             
              
     
               </Row>
       </div>);
  }
      

  render() {
    
    const {
      isLoading,
      token,
        qcm:[],
        usersession: {} 
        
      
    } = this.state;

    if (isLoading) {
      return(<div>
       
         
        
        </div>);
    }
    
    return (
    
      <div>

          {this.ListnewQcm()}
      </div>


    );

  }
}

export default Profile;






