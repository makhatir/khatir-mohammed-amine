 import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';

import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from 'reactstrap';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';

class EditP extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: {}
    }; 
     this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
      onChange(event) {
          const state = this.state.users;
    state[event.target.name] = event.target.value;
    this.setState({value: event.target.value});
  }

  onSubmit(event) {
    alert(' User : ' + this.state.users.nom + ' is update');
    event.preventDefault();
          const { nom, prenom, email, password} = this.state.users;

    axios.put('/api/account/users/'+this.props.match.params.userId, { nom, prenom, prenom, email, password})
      .then((result) => {
        this.props.history.push("/showP/"+this.props.match.params.userId)
      });
  }

      componentDidMount() {
    axios.get('/api/account/users/'+this.props.match.params.userId)
         
      .then(res => {
         console.log('eror');
        this.setState({ users: res.data });
        console.log(this.state.users);
         
      });
  }
 

 
  
 
  render() {

     const { nom, prenom, email, password} = this.state;

    return (
    
        
       <div className="container">
            
             <Card>
              <CardHeader>
                <div className="text-center">
                <span> EDIT  Your information personnel </span>
                  </div>
              </CardHeader>
              <CardBody className="text-center">
                <Form onSubmit={this.onSubmit} className="form-horizontal">
                <FormGroup row>
                    <Col md="12">
                    <div className="text-center user-pic ">
                    <img src="https://image.freepik.com/free-icon/male-user-shadow_318-34042.jpg"/>
                    <span className="display-4">{this.state.users.prenom} | {this.state.users.nom} </span>
                   
                      
                       </div>
                        </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label>Firstname</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" name="prenom" value={this.state.users.prenom} onChange={this.onChange} placeholder="Enter your Firstname..."/>
                      
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="hf-email">Lastname</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" name="nom" value={this.state.users.nom} onChange={this.onChange} placeholder="Enter your Lastname..."/>
                      
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="hf-email">Email</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" name="email" value={this.state.users.email} onChange={this.onChange} placeholder="example@gmail.com..." />
                      
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="hf-password">Password</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="password" id="hf-password" value={this.state.users.password} onChange={this.onChange} name="password" placeholder="Enter Password..."/>
  
                    </Col>
                  </FormGroup>
                   
                  <div className="text-center">
                  <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o" ></i> Save Change</Button>
                 
                 <Button to="/users/listUser" size="sm" color="danger" ><i className="fa fa-ban"></i> Cancel</Button>
               </div>
                </Form>
              </CardBody>
              
            </Card>
             
         </div>
     

       
    );
  }
}

export default EditP;
