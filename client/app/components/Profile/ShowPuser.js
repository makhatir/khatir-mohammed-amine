import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';
import $ from 'jquery';
import DataTable from 'datatables.net';
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
} from 'reactstrap';

class ShowP extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      users: {},
      qcms:[],
      categories:[],
         data:[],
  
    };

 
     
   
  }

      componentDidMount() {
    axios.get('/api/account/users/'+this.props.match.params.userId)
         
      .then(res => {
         console.log('eror');
        this.setState({ users: res.data });
          this.setState({
            data: this.state.users.qcmP
          })
        console.log(this.state.users);
        console.log('sucss',this.state.users.nom);
      });

      axios.get('/api/account/qcm')
      .then(res => {
        this.setState({ qcms: res.data });

        console.log(this.state.qcms);
      }); 
     
      axios.get('/api/account/categories')
      .then(res => {
        this.setState({ categories: res.data });

      
      }); 
  }
    

  
 
  render() {
    
      $(document).ready( function () {
      setTimeout(function(){
        $('#QcmP').DataTable();
      }, 400);
    });


  if (!this.state.users.userId) {
      return (
         <div>

         </div>
 

       
      );
    }
   
    if (this.state.users.userId) {
     
  
    return (
    
       <div className="container">

        <Row>
          <Col>
          <Card>
              <CardHeader>
                <div className="text-center">
                <span> Information User  </span>
                  </div>
              </CardHeader>
              <CardBody className="text-center">
                <Form  className="form-horizontal">
                <FormGroup row>
                    <Col md="12">
                    <div className="text-center user-pic ">
                    <img src="https://image.freepik.com/free-icon/male-user-shadow_318-34042.jpg"/>
                    <span className="display-4">{this.state.users.prenom}  {this.state.users.nom} </span>
                   
                      
                       </div>
                        </Col>
                  </FormGroup>
                  </Form>
                  </CardBody>
                  </Card>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Your information personnel
              </CardHeader>
              <CardBody>
                <Table hover bordered striped responsive size="sm">
                  <thead>
                  <tr>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th>Email</th>
                    <th>Role</th>
                  
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>{this.state.users.nom}</td>
                    <td>{this.state.users.prenom}</td>
                    <td>{this.state.users.email}</td>
                    <td>{this.state.users.type}</td>
                    
                    
                   
                  </tr>
               
                  
               
                  </tbody>
                </Table>
                 
              </CardBody>
           <CardFooter>
           <Link to={`/EditProfile/${this.state.users._id}`}  className="btn btn-sm btn-success"><i className="fa fa-pencil"></i> Edit</Link>
                   
           </CardFooter>
            </Card>
          </Col>
          <Col>
                <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Qcm Passer
              </CardHeader>
              <CardBody>
              <Table  id="QcmP" className="display" hover responsive className="table-outline mb-0 d-none d-sm-table">
                  <thead>
                  <tr>
                    <th>Qcm </th>
                    <th>Score</th>
                    <th>Date</th>
                 
                
                  </tr>
                  </thead>
                  <tbody>
                {
                  this.state.data.map(info=>
                  <tr>
                  <td>{info.Qcm}</td>
                  <td>{info.Score}</td>
                  <td>{info.dateP}</td>
                  </tr>
                  )
                }
                  </tbody>
                </Table>
                 
                 
              </CardBody>
           <CardFooter>
                   
           </CardFooter>
            </Card>
          </Col>
        </Row>
 
            </div> 
      

       
    );
  }
  }
}

export default ShowP;
