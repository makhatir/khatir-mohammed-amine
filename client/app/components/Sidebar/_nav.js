export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW'
      }
    },
    
    {
      title: true,
      name: 'Components',
      wrapper: {
        element: '',
        attributes: {}
      },
    },
    {
      name: 'Users',
      url: '/base',
      icon: 'icon-puzzle',
      children: [
        {
          name: 'Create User',
          url: '/users/createUser',
          icon: 'icon-puzzle'
        },
        {
          name: 'List Users',
          url: '/users/listUser',
          icon: 'icon-puzzle'
        }
      ]
    },
   
    {
      name: 'Test',
      url: '/addQcm/qcms',
      icon: 'icon-puzzle',
      children: [
        {
          name: 'Create New Test',
          url: '/addQcm/qcms',
          icon: 'icon-puzzle',
          badge: {
            variant: 'success',
            text: 'NEW'
          }
        },
        {
          name: 'List Tests',
          url: '/listqcms',
          icon: 'icon-puzzle',
          badge: {
            variant: 'secondary',
            text: '4.7'
          }
        }
      
      ]
    },


    {
      title: true,
      name: 'Extras'
    },
    {
      name: 'Pages',
      url: '/pages',
      icon: 'icon-star',
      children: [
        {
          name: 'wcshools',
          url: '/login',
          icon: 'icon-star'
        },
        {
          name: 'Register',
          url: '/register',
          icon: 'icon-star'
        },
        {
          name: 'Error 404',
          url: '/404',
          icon: 'icon-star'
        },
        {
          name: 'Error 500',
          url: '/500',
          icon: 'icon-star'
        }
      ]
    },
    {
      name: ' SQLI ',
      url: 'http://www.sqli.com',
      icon: 'icon-cloud-download',
      class: 'mt-auto',
      variant: 'warning'
    },
    {
      name: 'SQLI Carrières',
      url: 'https://www.sqli-carrieres.com/',
      icon: 'icon-layers',
      variant: 'secondary'
    }
  ]
};
