import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import $ from 'jquery';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'
 
import 'whatwg-fetch';
import axios from 'axios';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Jumbotron, Container
} from 'reactstrap';

import {
    Concours
} from './../StudentMode/Concours';
import {
    getFromStorage,
    setInStorage,
} from '../../utils/storage';
class ShowQcm extends Component {
  constructor(props) {
    super(props);
 
    this.state = {
      isLoading: true,
      qcms: {},
      qcmP:[],
      token: '',
      usersession:{},
      users:[],
      data:[],
      modal: false,
      large: false
      
  
    };
    this.toggle = this.toggle.bind(this);
    this.toggleLarge = this.toggleLarge.bind(this);
 
         this.LaunchQcm = this.LaunchQcm.bind(this);
   
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  toggleLarge() {
    this.setState({
      large: !this.state.large
    });
  }
      componentDidMount() {
        
    axios.get('/api/account/qcms/'+this.props.match.params._id)
         
      .then(res => {
         console.log('eror');
        this.setState({ qcms: res.data });
        console.log(this.state.qcms);
        console.log('sucss');
      });
  }
   
    
  LaunchQcm(event) {
    
    event.preventDefault();
         const obj = getFromStorage('the_main_app');
    if(obj && obj.token){
          const {token} = obj;
         fetch('/api/account/signin')
         .then(res => res.json()) 
    }

  if (obj && obj.token) {
    const {token} = obj;
        
    //Verify token
    fetch('/api/account/verify?token=' + token)
  
    .then(res => res.json())
    .then(json => {
      if (json.success) {
        this.setState({
          token,
          isLoading: false,
        });
        axios.get('/api/account/usersession/'+ token)
        .then(res => {
          
          this.setState({ usersession: res.data });
          this.setState({
            data: this.state.usersession
          })
        

       axios.get('/api/account/users/'+this.state.usersession.userId)
         
        .then(res => {
        
          this.setState({ users: res.data });
      
          console.log('users',this.state.users);


  
        const { 
            qcm,
            score,
            qcmP:[]
          } = this.state;
        var qcmP=this.state.users.qcmP;
          var arr={Qcm:this.state.qcms.titre,Score:'40' };
          
          qcmP.push(arr);
      axios.put('/api/account/users/'+this.state.usersession.userId, {qcmP,arr})
      .then((result) => {
          this.props.history.push("/appQcm/"+this.state.qcms._id)
      
      
      });
         
         
        });
       

        
        });
       
     
      } else {
        this.setState({
          isLoading: false,
        });
     
      }
    });
  } 
        
  }
 
 
  render() {

 
    const {
      isLoading,
      token,
      users:{},
      usersession:{},
      
    } = this.state;
 

    return (
    
 
        
       <div className="container">
       

          <Card>
          <CardHeader>
            <i className="fa fa-align-justify"></i><strong>
            
                 </strong>
                
          </CardHeader>
          <CardBody>
            <Jumbotron>
              
              <h1 className="display-3 text-center">Projet...{this.state.qcms.titre}</h1>
              <p className="lead text-center">{this.state.qcms.description}</p>
              <hr className="my-2"/>
              <p className="text-center"> Level is <span>{this.state.qcms.level}</span> </p>
              <div className="text-center">
            <span className="">
            <i className="fa fa-clock-o fa-lg mt-4"> {this.state.qcms.duree}  Min</i> 
                    </span>
            </div>
              <p className="text-center ml-1"> 
          <Link to={`/appQcm/${this.state.qcms._id}`} color="primary">
                <Button  color="primary" >
                     
                    Launch Test 
                    </Button></Link>

            
             
              </p>
             
             
            </Jumbotron>
          </CardBody>
        </Card> 
        
         <Modal isOpen={this.state.large} toggle={this.toggleLarge}
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader toggle={this.toggleLarge}>
                  <h4 className="text-center"> Qcm...{this.state.qcms.titre}</h4></ModalHeader>
                  <ModalBody>
                <Concours/>
                  </ModalBody>
                  <ModalFooter>
                       <Button color="secondary" onClick={this.toggleLarge}>Cancel</Button>
                  </ModalFooter>
                </Modal>
 
 

            </div> 
      

       
    );
  }
}

export default ShowQcm;
