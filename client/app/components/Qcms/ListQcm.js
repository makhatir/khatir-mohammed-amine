import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {Bar, Line} from 'react-chartjs-2';
import $ from 'jquery';
$.DataTable = DataTable;
import DataTable from 'datatables.net';
import {
  Badge,
  Row,
  Col,
  Progress,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Button,
  ButtonToolbar,
  ButtonGroup,
  ButtonDropdown,
  Label,
  Input,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from 'reactstrap';
 
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';


class ListQcm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      qcms: []
      
  
    };

   
   
  }

  componentDidMount() {
    axios.get('/api/account/qcm')
      .then(res => {
        this.setState({ qcms: res.data });
        console.log(this.state.qcms);
      }); 
      
    
  }
    delete(_id){
    console.log(_id);
       
    axios.delete('/api/account/qcms'+_id)
      .then((result) => {
        this.props.history.push("/ListQcm")
      });
  }


 
 
  render() {
    $(document).ready( function () {
      setTimeout(function(){
        $('#ListQcms').DataTable();
      }, 400);
    });
   

    return (
    
      <div>
       <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> List Qcm 
              </CardHeader>
              <CardBody>
              <Table id="ListQcms" className="display" hover responsive className="table-outline mb-0 d-none d-sm-table">
                  <thead>
                  <tr>
                    <th>Titre</th>
                    <th>Description</th>
                    <th>Duree</th>
                    <th>Score</th>
                    <th>Level</th>
                    <th>Categorie</th>
                    <th>auteur</th>
                    <th> Update</th>
                     <th>Operation</th>
                  </tr>
                  </thead>
                  <tbody>
                  {this.state.qcms.map(qcm =>
                  <tr>
                    <td scope="row"><Link to={`/showQcm/${qcm._id}`} >{qcm.titre}</Link></td> 
                    <td >{qcm.description}</td>
                    <td>{qcm.duree}</td>
                    <td>{qcm.score}</td>
                    <td>{qcm.level}</td>
                    <td>{qcm.categorie}</td>
                    <td>{qcm.auteur}</td>
                    <td> <Link to={`/addQuestion/${qcm._id}`} >Add Question</Link></td>
                     <td><Link to={`/deleteQcm/${qcm._id}`}><button  className="btn btn-danger">Delete</button></Link>
            <Link to={`/editQcm/${qcm._id}`}  className="btn btn-success">Edit</Link>

</td>
                  </tr>
                )}
                
                  </tbody>
                </Table>
                 
              </CardBody>
            </Card>
          </Col>
        </Row>
 
               
            
      </div>
    );
  }
}

export default ListQcm;
