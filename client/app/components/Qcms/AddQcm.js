import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'
 
import 'whatwg-fetch';
import axios from 'axios';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
import {
    Row,
    Col,
    Button,
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    CardHeader,
    CardFooter,
    CardBody,
    Collapse,
    Form,
    FormGroup,
    FormText,
    Label,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Jumbotron, Container
  } from 'reactstrap';
  
class AddQcm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      qcms: {},
      dataq:[],
      Question:'',
      Reponse:'',
      name: '',
      shareholders: [{ name: '' }]
      
  
    };
    this.onTextboxChangeQuestion = this.onTextboxChangeQuestion.bind(this);
    this.onTextboxChangeReponse = this.onTextboxChangeReponse.bind(this);
    this.AddQuestion = this.AddQuestion.bind(this);
 
       
 
     
   
  }
  onTextboxChangeQuestion(event) {
    this.setState({
      Question: event.target.value,
    });
  }
  onTextboxChangeReponse(event) {
    this.setState({
      Reponse: event.target.value,
    });
  }
      componentDidMount() {
    axios.get('/api/account/qcms/'+this.props.match.params._id)
         
      .then(res => {
         console.log('eror');
        this.setState({ qcms: res.data });
        console.log(this.state.qcms);
        console.log('sucss');
      });
  }
    
  AddQuestion(event) {
    alert(' Sous Question : ' + this.state.Question + ' is Added');
    event.preventDefault();
         
          const { 
            Question,
            Reponse,
          } = this.state;
          
          var dataq=this.state.qcms.questions;
          var arr={question:Question,reponse:Reponse};

          dataq.push(arr);

    axios.put('/api/account/qcms/'+this.props.match.params._id, {dataq,arr})
      .then((result) => {
        this.props.history.push("/editqcm/"+this.props.match.params._id)
      
        
      
      });
  }

    

  delete(_id){
    console.log(_id);
       
    axios.delete('/api/account/qcms/'+_id)
      .then((result) => {
        this.props.history.push("/#/addQcm/qcms")
      });
  }
 
  render() {

    const {
      Question,
      Reponse,
   
       
    } = this.state;

    return (
    
 
        
       <div>
          <Card>
          <CardHeader>
            <i className="fa fa-align-justify"></i><strong>
          
                 </strong>
                
          </CardHeader>
          <CardBody>
            <Jumbotron>
              <h1 className="display-3 text-center">Qcm...{this.state.qcms.titre}</h1>
              <p className="lead text-center">{this.state.qcms.description}</p>
              <hr className="my-2"/>
              <p className="text-center"> Level is <span>{this.state.qcms.level}</span> </p>
              <div className="text-center">
            <span className="">
            <i className="fa fa-clock-o fa-lg mt-4"> {this.state.qcms.duree}  Min</i> 
            
        
            </span>
            </div>
             
            </Jumbotron>
          </CardBody>
        </Card> 
        <Col xs="12" md="6">
           <Card>
              <CardHeader>
                <strong>Add Question </strong>
                
              </CardHeader>
              <CardBody>
                <Form  className="form-horizontal" >
                 
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Question</Label>
                    </Col>
                    <Col xs="12" md="9">
            <Input type="text" name="titre" value={Question} onChange={this.onTextboxChangeQuestion} required placeholder="titre" />
                      
                      <FormText color="muted">Entrer Sous categorie n'existe pas </FormText>
                    </Col>
                  </FormGroup>
                     <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="textarea-input">Reponse</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="textarea" name="description" value={Reponse} onChange={this.onTextboxChangeReponse} required rows="9"
                             placeholder="Content..."/>
                    </Col>
                
                 
                 
                  </FormGroup>
    
                </Form>
              </CardBody>
              
              <CardFooter>
              <div className="text-center"> <Button type="submit"onClick={this.AddQuestion}  className="btn btn-primary"><i className="fa fa-dot-circle-o"></i> Add </Button>
              
            
               </div>
              </CardFooter>
              </Card>
          </Col>
            </div> 
      

       
    );
  }
}

export default AddQcm;
