import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';

import {
    Row,
    Col,
    Button,
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    CardHeader,
    CardFooter,
    CardBody,
    Collapse,
    Form,
    FormGroup,
    FormText,
    Label,
    Input,
    Table,
    InputGroup,
    InputGroupAddon,
    InputGroupText
  } from 'reactstrap';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';

class EditQcm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      qcms: {},
      questions:[],
      reponse:[],
      Id:[],
      Question:'',
      Reponse:'',
      data:[],
    
    }; 
     this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onTextboxChangeQuestion = this.onTextboxChangeQuestion.bind(this);
    this.onTextboxChangeReponse = this.onTextboxChangeReponse.bind(this);
    this.AddQuestion = this.AddQuestion.bind(this);
    this.AddReponse = this.AddReponse.bind(this);
    this.DeleteQuest = this.DeleteQuest.bind(this);
    
    
  }

  onTextboxChangeQuestion(event) {
    this.setState({
      Question: event.target.value,
    });
  }
  onTextboxChangeReponse(event) {
    this.setState({
      Reponse: event.target.value,
    });
  }
      onChange(event) {
          const state = this.state.qcms;
    state[event.target.name] = event.target.value;
    this.setState({value: event.target.value});
  }

  onSubmit(event) {
    alert(' Qcm : ' + this.state.qcms.titre + ' is update');
    event.preventDefault();
          const { titre, description, duree,score,src, level, categorie,questions} = this.state.qcms;

    axios.put('/api/account/qcms/'+this.props.match.params._id, { titre, description, duree,score,src, level, categorie,questions})
      .then((result) => {
        this.props.history.push("/editQcm/"+this.props.match.params._id)
      });
  }
  


  AddQuestion(event) {
    alert(' question : ' + this.state.Question + ' is Added');
    event.preventDefault();
         
          const { 
            Question,
            Reponse,
                } = this.state;
          
          var questions=this.state.qcms.questions;
        var newId=this.state.Id;
          var arr={id:newId+1,question:Question,correct:'c',reponse:{id:'c',rep:Reponse}};

          questions.push(arr);

    axios.put('/api/account/qcms/'+this.props.match.params._id, {questions,arr})
      .then((result) => {
        this.props.history.push("/editqcm/"+this.props.match.params._id)
      
        
      
      });
  }

  AddReponse(event) {
    alert(' reponse : ' + this.state.Reponse + ' is Added');
    event.preventDefault();
         
          const { 
            Reponse,
                } = this.state;
          
          var reponse=this.state.REPO.reponse;
        
          var arr={rep:Reponse};

          reponse.push(arr);

    axios.put('/api/account/qcms/'+this.props.match.params._id, {reponse,arr})
      .then((result) => {
        console.log(reponse,arr)
        this.props.history.push("/editqcm/"+this.props.match.params._id)
      
        
      
      });
  }
  DeleteQuest(event) {
   
    event.preventDefault();
         
          const { 
            Question,
                } = this.state;
          
          var questions=this.state.qcms.questions;
          
          var arr={question:Question};
          
          
          questions.splice(0,1);
          

    axios.put('/api/account/qcms/'+this.props.match.params._id, {questions,arr})
      .then((result) => {
        this.props.history.push("/editqcm/"+this.props.match.params._id)
      
        
      
      });
      
          
      
   
  }
      componentDidMount() {
    axios.get('/api/account/qcms/'+this.props.match.params._id)
         
      .then(res => {
         console.log('eror');
        this.setState({ qcms: res.data });
        console.log(this.state.qcms['questions']);
        this.setState({
          data: this.state.qcms['questions']
        })
        var numb =this.state.data.length;
        this.setState({
          Id: this.state.qcms['questions'][numb-1].id
        })
        this.setState({
          REPO: this.state.qcms.questions[0]
        })
        
        console.log('Id',this.state.Id);
        console.log('reponse',this.state.REPO[0].reponse);
        
        
        console.log('data',this.state.data);
        var cat =this.state.data;
        cat.map((info,i) => 
        info.reponse.map(inf => 
         
          console.log('reep',inf.rep)
        )
   
        )
    
      });
  }
 

 
  render() {

     const { titre, description, duree,score,src, level, categorie,questions, Question,
      Reponse, 
    } = this.state;
    var cat =this.state.data;
    return (
    
     
       <div>
             <h2 className="text-center"> Edit Qcm  </h2>
        <Row>  <Col xs="12" md="12">
             <CardBody>
        <Form onSubmit={this.onSubmit}>
        <FormGroup row>
        <Col md="3">
                <Label >Titre:</Label>
                </Col>
                <Col xs="12" md="9">
                <Input type="text"   name="titre" value={this.state.qcms.titre} onChange={this.onChange} placeholder="titre" />
                </Col>
                </FormGroup>
                <FormGroup row>
                <Col md="3">
                <Label for="description">Description:</Label>
                </Col>
                <Col xs="12" md="9">
                <Input type="text"  name="description" value={this.state.qcms.description} onChange={this.onChange} placeholder="prenom" />
                </Col>
                </FormGroup>
                <FormGroup row>
                <Col md="3">
                <Label for="duree">Duree:</Label>
                </Col>
                <Col xs="12" md="9">
                <Input type="text"   name="duree" value={this.state.qcms.duree} onChange={this.onChange} placeholder="duree" />
                </Col>
                </FormGroup>
                <FormGroup row>
                <Col md="3">
                <Label for="Score">Score:</Label>
                </Col>
                <Col xs="12" md="9">
                <Input type="text"   name="score" value={this.state.qcms.score} onChange={this.onChange} placeholder="score" />
                </Col>
                </FormGroup>
                <FormGroup row>
                <Col md="3">
                <Label for="Src">Src:</Label>
                </Col>
                <Col xs="12" md="9">
                <Input type="text"   name="src" value={this.state.qcms.src} onChange={this.onChange} placeholder="Src" />
                </Col>
                </FormGroup>
                <FormGroup row>
                <Col md="3">
                <Label for="level">Level:</Label>
                </Col>
                <Col xs="12" md="9">
                <Input type="text"  name="level" value={this.state.qcms.level} onChange={this.onChange} placeholder="level" />
                </Col>
                </FormGroup>
                <FormGroup row>
                <Col md="3">
                <Label for="questions">Question:</Label>
                </Col>
                <Col xs="12" md="9">
                <Input type="text"   name="questions" value={this.state.qcms.questions} onChange={this.onChange} placeholder="questions" />
                 </Col>
                 </FormGroup>
              <CardFooter className="text-center">
               
              <Button type="submit" className="btn btn-default">Submit</Button>
              </CardFooter>
            </Form>
            </CardBody>
            </Col>
            <Col xs="12" md="6">
           <Card>
              <CardHeader>
                <strong>Add Question </strong>
                
              </CardHeader>
              <CardBody>
                <Form  className="form-horizontal" >
                 
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Question</Label>
                    </Col>
                    <Col xs="12" md="9">
            <Input type="text" name="titre" value={Question} onChange={this.onTextboxChangeQuestion} required placeholder="titre" />
                      
                      <FormText color="muted">Entrer Sous categorie n'existe pas </FormText>
                    </Col>
                  </FormGroup>
                     <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="textarea-input">Reponse</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="textarea" name="description" value={Reponse} onChange={this.onTextboxChangeReponse} required rows="9"
                             placeholder="Content..."/>
                    </Col>
                
                 
                 
                  </FormGroup>
    
                </Form>
              </CardBody>
              
              <CardFooter>
               <div className="text-center"> 
               <Button type="submit"onClick={this.AddQuestion}  className="btn btn-primary"><i className="fa fa-dot-circle-o"></i> Add Question</Button>
               <Button type="submit"onClick={this.AddReponse}  className="btn btn-primary"><i className="fa fa-dot-circle-o"></i> Add Reponse</Button>
               <Button type="submit"onClick={this.DeleteQuest}  className="btn btn-primary"><i className="fa fa-dot-circle-o"></i> DeleteQuest</Button>
              
              </div>
                
               
              </CardFooter>
              </Card>
          </Col>
          
<Col xs="12" md="6">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> list Question
              </CardHeader>
              <CardBody>
                <Table id="ListCategorie" responsive bordered>
                  <thead>
                  <tr>
                  <th>Question</th>
                  <th>Reponse</th>
                  <th>Operation</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                 
  {cat.map(info =>
    <tr>
 <th scope="row"><Link to={`/ShowQst/${info._id}`} > {info.question} </Link></th>
         
 
<td>      {info.reponse.map(inf =>    
          
        <span> {inf.rep}</span> 
        )} </td>
<td>delete/edit</td>
      
 
    </tr>
  )}
                  </tbody>
                </Table>
                 
              </CardBody>
            </Card>
          </Col>
          </Row>
      </div>

       
    );
  }
}

export default EditQcm;
