import React, {Component} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
 
 
import {Container} from 'reactstrap';
 
import $ from 'jquery';
import 'whatwg-fetch';
 
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  Form,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from 'reactstrap';
  

 
 

 

 
import { IndexRoute } from 'react-router';
    import ListQcm from './ListQcm'; 
 
import 'whatwg-fetch';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
import axios from 'axios';

class Qcms extends Component {
    constructor(props) {
    super(props);
 this.state = {
         isLoading: true,
      token: '',
      addQcmError: '',
      Titre: '',
      Desc: '',
      Duree: '',
      Score:'',
      Src:'',
      Level: '',
      Auteur:'',
      name: '',
      shareholders: [{ name: '' }],
      usersession: {} 
    };
   
 
 
    this.onTextboxChangeTitre = this.onTextboxChangeTitre.bind(this);
    this.onTextboxChangeDesc = this.onTextboxChangeDesc.bind(this);
    this.onTextboxChangeDuree = this.onTextboxChangeDuree.bind(this);
    this.onTextboxChangeScore = this.onTextboxChangeScore.bind(this);
    this.onTextboxChangeSrc = this.onTextboxChangeSrc.bind(this);
   
    this.onTextboxChangeLevel = this.onTextboxChangeLevel.bind(this);
     
        
    this.addQcm = this.addQcm.bind(this);
   
 
 
   
  
   
  }

  componentDidMount() {
  
    const obj = getFromStorage('the_main_app');
    if(obj && obj.token){
          const {token} = obj;
         fetch('/api/account/signin')
         .then(res => res.json()) 
    }

  if (obj && obj.token) {
    const {token} = obj;
        
    //Verify token
    fetch('/api/account/verify?token=' + token)
  
    .then(res => res.json())
    .then(json => {
      if (json.success) {
        this.setState({
          token,
          isLoading: false,
        });
        axios.get('/api/account/usersession/'+ token)
        .then(res => {
           console.log('eror');
          this.setState({ usersession: res.data });
       
        });
      } else {
        this.setState({
          isLoading: false,
        });
        console.log('eerrrr')
      }
    });
  } else {
    this.setState({
      isLoading: false,
    });
  }
 
}
  
 
 

onTextboxChangeTitre(event) {
  this.setState({
    Titre: event.target.value,
  });
}

onTextboxChangeDesc(event) {
  this.setState({
    Desc: event.target.value,
  });
}
    onTextboxChangeDuree(event) {
  this.setState({
    Duree: event.target.value,
  });
}
onTextboxChangeScore(event) {
  this.setState({
    Score: event.target.value,
  });
}
onTextboxChangeSrc(event) {
  this.setState({
    Src: event.target.value,
  });
}
    onTextboxChangeLevel(event) {
  this.setState({
    Level: event.target.value,
  });
}

 
addQcm() {
  // Grab state
  const {
    Id,
    Titre,
    Desc,
   Duree,
   Score,
   Src,
    Level,
 
         } = this.state;

  this.setState({
    isLoading: true,
  });
  // Post request to backend
  fetch('/api/account/qcms', { 
    method: 'POST',
     headers: {
       'Content-Type': 'application/json'
     },
    body: JSON.stringify({
      auteur:this.state.usersession.username,
      _id:Id,
      titre: Titre,
      description: Desc,
      duree: Duree,
      score: Score,
      src:Src,
      level: Level,
     
    }),
    
  }).then(res => res.json())
    .then(json => {
      
      if(json.success){
          this.setState({
          addQcmError: json.message,
          isLoading: false,
          Id: '',
          Titre: '',
          Desc:'',
          Duree:'',
          Score:'',
          Src:'',
          Level:'',
 
        });
        
      } else {
        this.setState({
          addQcmError: json.message,
          isLoading: false,
        });
        
      } 
    });
    
}
   



 
  render() {
    const {
      isLoading,
      token ,
      Titre,
      Desc,
      Duree,
      Score,
      Src,
      Level,
     Question,
    usersession:{},
     
      addQcmError
  
      
    } = this.state;

    if (isLoading) {
      return(<div><p>Loading...</p></div>);
    }
    if (!token) {
      return (
        <div>
   <Redirect to="/login"/>

        </div>
      );
    }

    return (
      <div className="app">
  
  
  <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader>
                <h2 className="text-center"> Qcm :{Titre} </h2>
             {
              (addQcmError) ? (
                <h5> {addQcmError}</h5>
              ) : (null)
            }
              </CardHeader>
              <CardBody>
                <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                    <FormGroup row>
                    <Col md="3">
                      <Label>Auteur</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <p className="form-control-static">

                      {this.state.usersession.username}</p>
                    </Col>
                  </FormGroup>
               
             
                  <FormGroup row>
                        
                    <Col md="3">
                      <Label htmlFor="text-input">Titre</Label>
                    </Col>
                    <Col xs="12" md="9">
                       
               <Input 
                type="text" 
                placeholder="Titre"
                value={Titre}
              onChange={this.onTextboxChangeTitre}
              required/>
                      <FormText color="muted">Entrer un Titre n'existe pas </FormText>
                    </Col>
                  </FormGroup>
           
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="textarea-input">Description</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input 
                type="textarea" 
            id="textarea-input" 
                rows="9"
                    value={Desc}
              onChange={this.onTextboxChangeDesc}
              required
                             placeholder="Content..."/>
         
                    </Col>
                  </FormGroup>
                <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Durée</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input 
                type="text" 
                id="text-input" 
                placeholder="Durée"
                value={Duree}
              onChange={this.onTextboxChangeDuree}
              required/>
          
  
 
                      <FormText color="muted">La durée en minute</FormText>
                    </Col>
                  </FormGroup>
                 
                      <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Score </Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input 
                type="text" 
                id="text-input" 
                placeholder="Score"
                value={Score}
              onChange={this.onTextboxChangeScore}
              required/>
                      </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="text-input">Src </Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input 
                type="text" 
                id="text-input" 
                placeholder="Src"
                value={Src}
              onChange={this.onTextboxChangeSrc}
              required/>
                      </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="select">Select Level</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="select" name="select" id="select" value={Level} onChange={this.onTextboxChangeLevel} required>
                        <option value="#">Please select</option>
                        <option value="easy" required >easy</option>
                        <option value="medium" required>medium</option>
                        <option value="hard" required >hard</option>
                        <option value="hardest" required >hardest</option>
                      </Input>
                    </Col>
                  </FormGroup>
                  <CardFooter className="text-center">
                
                <Button type="submit" onClick={this.addQcm} size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Create Qcm</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
              </CardFooter>
                  </Form>
                 
    
              </CardBody>
              
            </Card>
       
          </Col>
        </Row>
      
       
       
      <ListQcm />
     
    </div>
    );
  }
}

export default Qcms;
