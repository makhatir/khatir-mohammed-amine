import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {Form, Table } from 'semantic-ui-react';
import {
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  FormGroup,
  FormText,
  Label,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from 'reactstrap';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';

class Create extends Component {
  constructor(props) {
    super(props);

    this.state = {
     nom: '',
      prenom: '',
      email: '',
      password: '',
      type: ''

    }; 
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
          onChange(event) {
          const state = this.state;
    state[event.target.name] = event.target.value;
    this.setState(state);
  }

  onSubmit(event) {
   
    event.preventDefault();
          const { nom, prenom, email, password, type='admin' } = this.state;
      
            axios.post('/api/account/signup', { nom, prenom, email, password,type})
      .then((result) => {
        this.props.history.push("/users/listUser")
      });
  }
 
  render() {
      const { nom, prenom, email, password,type } = this.state;
   

    return (
    
        <div className="profil">
       <div className="container">
       
        <Card>
              <CardHeader>
              <div className="text-center">
      <Link to="/users/listUser" className="btn btn-sm btn-primary">
        List Users</Link> </div>
              </CardHeader>
              <CardBody>
                <Form onSubmit={this.onSubmit} className="form-horizontal">
                <FormGroup row>
                    <Col md="12">
                    <div className="col-md-12 user-pic ">
                      <img src="https://image.freepik.com/free-icon/male-user-shadow_318-34042.jpg"/>
                      <span className="display-4">Add User : {prenom}  {nom}  </span>
                    </div>
                  
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label>Firstname</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" name="prenom" value={prenom} onChange={this.onChange} placeholder="Enter your Firstname..."/>
                      
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="hf-email">Lastname</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" name="nom" value={nom} onChange={this.onChange} placeholder="Enter your Lastname..."/>
                      
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="hf-email">Email</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" name="email" value={email} onChange={this.onChange} placeholder="example@gmail.com..." />
                      
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="hf-password">Password</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="password" id="hf-password" value={password} onChange={this.onChange} name="password" placeholder="Enter Password..."/>
  
                    </Col>
                  </FormGroup>
                   
                 <div className="text-center">
                  <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                  <Button to="/users/listUser" size="sm" color="danger" ><i className="fa fa-ban"></i> Cancel</Button>
               </div>
                </Form>
              </CardBody>
              <CardFooter>
                
              </CardFooter>
            </Card>
        
        
            </div> 
      </div>

       
    );
  }
}

export default Create;
