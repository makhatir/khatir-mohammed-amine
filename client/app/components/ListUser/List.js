import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {Bar, Line} from 'react-chartjs-2';
import $ from 'jquery';
import DataTable from 'datatables.net';

import {
  Badge,
  Row,
  Col,
  Progress,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Button,
  ButtonToolbar,
  ButtonGroup,
  ButtonDropdown,
  Label,
  Input,
  Table,
  Modal,
  ModalHeader,
  ModalBody, 
  ModalFooter,
} from 'reactstrap';
import Show from './Show';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
$.DataTable = DataTable;

class ListUser extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      isLoading: true,
      users: []
    };

   
  }
 

  
   
  componentDidMount() {
    axios.get('/api/account/users')
      .then(res => {
        this.setState({ users: res.data });
     
      }); 
  }
  
 

  render() {
    $(document).ready( function () {
      setTimeout(function(){
        $('#ListUser').DataTable();
      }, 400);
    });

    return (
      <div>
      
        <div className="page-content">
        <span className="display-4 text-center">  LIST USERS </span>
        <div className="text-center">  
        <Link to="/users/createUser" className="btn btn-sm btn-success"><i className="fa fa-plus"></i> Add User</Link></div>
          <Table id="ListUser" className="display" hover responsive className="table-outline mb-0 d-none d-sm-table">
            <thead className="thead-light">
              <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Email</th>  
                <th>Type</th>
                <th>Operation</th>
              </tr>
              </thead>
            <tbody>      
            {this.state.users.map(user =>
              <tr key={user._id}>
                <th  scope="row"><Link to={`/users/showUser/${user._id}`} >{user.prenom}</Link></th> 

                <th>{user.nom}</th>
                <th>{user.email}</th>
                <th>{user.type}</th>
                  <th>
                    <Link  to={`/users/showUser/${user._id}`}  className="btn btn-sm btn-primary"><i className="fa fa-pencil"></i> Show</Link>
                    <Link to={`/users/editUser/${user._id}`}  className="btn btn-sm btn-success"><i className="fa fa-pencil"></i> Edit</Link>
                    <Link to={`/users/deleteUser/${user._id}`}  className="btn btn-sm btn-danger"><i className="fa fa-pencil"></i> Delete</Link>
                    
                      </th>
              </tr>
            )}
            </tbody>
          </Table>     
          </div>
           
        </div>
      );
      return(
        <div>
                
        </div>
      );
    }
  
}
export default ListUser;