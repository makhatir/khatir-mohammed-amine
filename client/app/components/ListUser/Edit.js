 import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
 
import $ from 'jquery';
import {
  Alert,
  Row,
  Col,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Collapse,
  FormGroup,
  FormText,
  Label,
  Input,
  Form, Table,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from 'reactstrap';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';

class Edit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: {},
      isGoing: true,
      
   
    }; 
   
    this.handleInputChange = this.handleInputChange.bind(this);
     this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  handleInputChange(event) {
    const state = this.state.users.type;
    const target = event.target;

    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name=target.name;
    state[target.name] = target.value;

    this.setState({value: target.value});
  }

      onChange(event) {
          const state = this.state.users;
    state[event.target.name] = event.target.value;
    this.setState({value: event.target.value});
  }
  
  
  onSubmit(event) {
    
    alert(' USER : ' + this.state.users.nom+ ' is Update');
   
    event.preventDefault();
          const { nom, prenom, email, password, type } = this.state.users;

    axios.put('/api/account/users/'+this.props.match.params.userId, { nom, prenom, prenom, email, password,type })
      .then((result) => {
        this.props.history.push("/users/listUser")
      });
  }

      componentDidMount() {
    axios.get('/api/account/users/'+this.props.match.params.userId)
         
      .then(res => {
         console.log('eror');
        this.setState({ users: res.data });
        console.log(this.state.users);
        console.log(this.state.users.type);
        var type =this.state.users.type;
        var x = document.getElementById("users");
        var y = document.getElementById("admins");

        type.map((t,i)=>
      {
        if(t=='admin')
        {
          x.style.display = "block";
          y.style.display = "none";
        }
        else{
          x.style.display = "none";
          y.style.display = "block";
        }
      }
          )
        
             
       
       
    }
      );


    }

    switch(){
      
    }
    
  render() {
  
     const { nom, prenom, email, password} = this.state;
      
     
      
    
  
    return (
    
        <div className="profil">
         
       <div className="container">
            
            <Card>
              <CardHeader>
                <div className="text-center">
              <Link to="/users/listUser" className="btn btn-sm btn-primary">
                List Users</Link></div>
              </CardHeader>
              <CardBody className="text-center">
                <Form onSubmit={this.onSubmit} className="form-horizontal">
                <FormGroup row>
                    <Col md="12">
                    <div className="text-center user-pic ">
                    <img src="https://image.freepik.com/free-icon/male-user-shadow_318-34042.jpg"/>
                    <span className="display-4">{this.state.users.prenom} | {this.state.users.nom} </span>
                   
                      
                       </div>
                        </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label>Firstname</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" name="prenom" value={this.state.users.prenom} onChange={this.onChange} placeholder="Enter your Firstname..."/>
                      
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="hf-email">Lastname</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" name="nom" value={this.state.users.nom} onChange={this.onChange} placeholder="Enter your Lastname..."/>
                      
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="hf-email">Email</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" name="email" value={this.state.users.email} onChange={this.onChange} placeholder="example@gmail.com..." />
                      
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="hf-password">Password</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="password" id="hf-password" value={this.state.users.password} onChange={this.onChange} name="password" placeholder="Enter Password..."/>
  
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label>Switch To  </Label>
                
                    </Col>
                    <p id="admins">
                    <Col md="1">
                    <Label> Admin </Label>
                    </Col>
                    <Col md="1">

                    <Label className="switch switch-default switch-pill switch-success-outline-alt">
                      <Input type="checkbox"  label="Admin" name="type" value="admin" checked={this.state.users.type === 'admin'}
                        onChange={this.onChange} className="switch-input" />
                      <span className="switch-label"></span>
                      <span className="switch-handle"></span>
                    </Label>
                    </Col>
            </p>
                 <p id="users">
                    <Col md="1">
                    <Label> User </Label>
                    </Col>
                    <Col md="3">
                    <Label className="switch switch-default switch-pill switch-success-outline-alt">
                      <Input type="checkbox"  label="User" name="type" value="user" checked={this.state.users.type === 'user'}
                        onChange={this.onChange} className="switch-input" />
                      <span className="switch-label"></span>
                      <span className="switch-handle"></span>
                    </Label>
                      
            </Col>
      </p>
     
                  </FormGroup>
                  <div className="text-center">
                  <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o" ></i> Save Change</Button>
                 
                 <Button to="/users/listUser" size="sm" color="danger" ><i className="fa fa-ban"></i> Cancel</Button>
               </div>
                </Form>
              </CardBody>
              
            </Card>
         </div>
      </div>
 
       
    );
  
  }
}

export default Edit;
