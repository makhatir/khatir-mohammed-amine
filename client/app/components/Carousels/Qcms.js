import React, {Component} from 'react';
import ListQcm from './../Qcms/ListQcm'
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';
import 'whatwg-fetch';
import axios from 'axios';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';

import {
    BrowserRouter as Router,
    Link,
    Switch
  } from 'react-router-dom'
 
const items = [];
  
class Carousels extends ListQcm {
    componentDidMount() {
        axios.get('/api/account/qcm')
          .then(res => {
            this.setState({ qcms: res.data });
            console.log(this.state.qcms);
         
          }); 
          
        
      }
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);

    this.state = {
        qcms: [],
        activeIndex: 0
        
    
      };
 
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }
  //
  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === this.state.qcms.length - 1 ? 0: this.state.activeIndex + 1;
    this.setState({activeIndex: nextIndex});
  }
  

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? this.state.qcms.length - 1 : this.state.activeIndex - 1;
    this.setState({activeIndex: nextIndex});
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({activeIndex: newIndex});
  }

  render() {
    const {activeIndex,qcms} = this.state;
    const items =  this.state.qcms;
 

    const slides2 = items.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.src}
        >
          <img className="d-block w-100" id="img-slides2"src={item.src} alt={item.titre}/>
          <CarouselCaption captionText={item.description} captionHeader={ <Link to={`/showQcm/${item._id}`} >{item.titre}</Link>}>
         
          </CarouselCaption>
        </CarouselItem>
      );
    });

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" xl="12">
            <Card>
              <CardBody>
                <Carousel activeIndex={activeIndex} next={this.next} previous={this.previous}>
                  <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex}/>
                  {slides2}
                  <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous}/>
                  <CarouselControl direction="next" directionText="Next" onClickHandler={this.next}/>
                </Carousel>
                  </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Carousels;