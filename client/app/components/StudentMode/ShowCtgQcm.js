import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  Jumbotron,
  FormGroup,
  Label,
  Input,Button
} from 'reactstrap';
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'
import $ from 'jquery';
import DataTable from 'datatables.net';

import 'whatwg-fetch';
import axios from 'axios';
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';

class ShowCtg extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      categories: {},
      qcms: [],
      sCtg:[]
 
  
    };
 
  }

      componentDidMount() {
    axios.get('/api/account/categories/'+this.props.match.params._id)
         
      .then(res => {
       
         console.log('eror');
        this.setState({ categories: res.data });
        console.log('titre-----',this.state.categories.titre);
        this.setState({
            sCtg: this.state.categories['sous_categorie']
          })
         
       
    
      });
      axios.get('/api/account/qcm')
      .then(res => {
        this.setState({ qcms: res.data });
   
        console.log(this.state.qcms);
        console.log(this.state.qcms[0].categorie);
      });
      
 
  }
    
  

 
 
  render() {
   
    $(document).ready( function () {
        setTimeout(function(){
          $('#ListSousCategorie').DataTable();

          
        }, 400);
        
       
      });
  
      var ctg =this.state.categories.titre;
      var data=this.state.qcms;
      console.log('ctg',ctg);
      console.log('data',this.state.qcms);
      data.map((info,i)=>{
          if(info.categorie === ctg)
            console.log(info.titre);
           
       }
  
  
  ) 
  
    return (
    
 
        
       <div className="container">
       <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Categorie:  {this.state.categories.titre}  
                <div className="text-center">  <Link to="/listCategories" className="btn btn-success">
                 List Categories</Link></div>
              </CardHeader>
              <CardBody>
              <Jumbotron>
              <h1 className="display-3 text-center">Categorie...{this.state.categories.titre}</h1>
              <p className="lead text-center">{this.state.categories.description}</p>
              <hr className="my-2"/>
              
         
              <FormGroup row>
                    <Col md="3">
                 
                      <Label htmlFor="select"> Sous Categorie</Label>
                    </Col>
                    <Col xs="5" md="5">
                      <Input type="select" value="Sous Categorie"   onChange= {this.onTextboxChangeCategorie} required>
                         <option>Select Sous Categorie</option>
                         {this.state.sCtg.map(t =><option> {t.titre_Sctg}</option>)}
                      </Input>
                    </Col>
                  </FormGroup>
                
              
            </Jumbotron>
                <Table hover bordered striped responsive size="sm">
                  <thead>
                 
                  </thead>
                  <tbody>
                 
                 
                  </tbody>
                  
                </Table>
                 
              </CardBody>
            </Card>
          </Col>
          <Col xs="12" lg="12">
       
       <Card>   
    <CardHeader>
      
    <i className="fa fa-align-justify"></i> List Qcms  
                 
    </CardHeader>
      <CardBody>
      
    <Table id="ListSousCategorie" responsive bordered>
                  <thead>
                  <tr>
                  <th>Titre</th>
                  <th>Description</th>
                  <th> Duree </th>
                  <th>Level </th>
                  <th>Categorie </th>
                  </tr>
                  </thead>
                  <tbody>
                 {   data.map((info,i)=>{
          if(info.categorie === ctg)
                     return   <tr>
                    <th scope="row"><Link to={`/showQcm/${info._id}`}>{info.titre}</Link></th>
                        <td> {info.description} </td>
                        <td>{info.duree}</td>
                        <td>{info.level}</td>
                        <td>{info.categorie}</td>
                     </tr>
           
       }
    
  
  ) 
}                
                    </tbody>
                </Table>
                </CardBody>
                </Card>
          </Col>
        </Row>
         

  
          </div> 
      

       
    );
  }
}

export default ShowCtg;
