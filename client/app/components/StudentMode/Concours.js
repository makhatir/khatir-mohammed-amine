import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {Bar, Line} from 'react-chartjs-2';
import $ from 'jquery';
import DataTable from 'datatables.net';
import Carousel from './../Carousels/Qcms';

import {
  Badge,
  Row,
  Col,
  Progress,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Button,
  ButtonToolbar,
  ButtonGroup,
  ButtonDropdown,
  Label,
  Input,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from 'reactstrap';
 
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';
 
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
 
 
 
class Concours extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      qcms: []
      
  
    };

 
     
   
  }

  

  componentDidMount() {
    axios.get('/api/account/qcm')
      .then(res => {
        this.setState({ qcms: res.data });
        console.log(this.state.qcms);
      
      }); 
      
    
  }

 
  render() {
    $(document).ready( function () {
      setTimeout(function(){
        $('#ListQcms').DataTable();
      }, 400);
    });
    return (
    
      <div>
        
        <Row>
       
           <Col>
                  <Carousel />
                  </Col>
                  </Row>
                <Row>
                
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> LIST QCMS 
                
              </CardHeader>
              <CardBody>
                <Table id="ListQcms" hover bordered striped responsive size="sm">
                  <thead>
                  <tr>
                  
                  </tr>
                  </thead>
                  <tbody>
                <td>Concours </td>
   
                 
                  </tbody>
                </Table>
                
              </CardBody>
            </Card>
          </Col>
        </Row>
 
      </div>
    );
  }
}

export default Concours;
