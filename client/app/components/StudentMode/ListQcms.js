import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import {Bar, Line} from 'react-chartjs-2';
import $ from 'jquery';
import DataTable from 'datatables.net';
import Carousel from './../Carousels/Qcms';

import {
  Badge,
  Row,
  Col,
  Progress,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Button,
  ButtonToolbar,
  ButtonGroup,
  ButtonDropdown,
  Label,
  Input,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from 'reactstrap';
 
import {
  BrowserRouter as Router,
  Link,
  Switch
} from 'react-router-dom'

import 'whatwg-fetch';
import axios from 'axios';
 
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
 
 
 
class ListQcms extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      qcms: []
      
  
    };

 
     
   
  }

  

  componentDidMount() {
    axios.get('/api/account/qcm')
      .then(res => {
        this.setState({ qcms: res.data });
        console.log(this.state.qcms);
      
      }); 
      
    
  }

 
  render() {
    $(document).ready( function () {
      setTimeout(function(){
        $('#ListQcms').DataTable();
      }, 400);
    });
    return (
    
      <div>
        
        <Row>
       
           <Col>
                  <Carousel />
                  </Col>
                  </Row>
                <Row>
                
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> LIST QCMS 
                
              </CardHeader>
              <CardBody>
                <Table id="ListQcms" hover bordered striped responsive size="sm">
                  <thead>
                  <tr>
                    <th>Titre</th>
                    <th>Description</th>
                    <th>Duree</th>
                    <th>Level</th>
                    <th>Categorie</th>
                    <th>Auteur</th>
                  </tr>
                  </thead>
                  <tbody>
                  {this.state.qcms.map(qcm =>
                  <tr>
                    <th scope="row"><Link to={`/showQcm/${qcm._id}`} >{qcm.titre}</Link></th> 
                    <th>{qcm.description}</th>
                    <th>{qcm.duree}</th>
                    <th>{qcm.level}</th>
                    <th>{qcm.categorie}</th>
                    <th>{qcm.auteur}</th>
                   
         
                  </tr>
          )}
   
                 
                  </tbody>
                </Table>
                
              </CardBody>
            </Card>
          </Col>
        </Row>
 
      </div>
    );
  }
}

export default ListQcms;
