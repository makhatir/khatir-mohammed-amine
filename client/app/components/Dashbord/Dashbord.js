import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import { BrowserRouter as Router} from 'react-router-dom';
import Auth from '../Auth/Auth';
 
 
import 'whatwg-fetch';

import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';

class Dashbord extends Auth {

 
  render() {
    const {
      isLoading,
      token
     
    } = this.state;

    if (isLoading) {
      return(<div><p>Loading...</p></div>);
    }
    if (!token) {
      return (
        <div>
            <Redirect to="/"/>
        </div>
      );
    }

    return (
 
        
      <div className="dash">
   

        <div className="container">
            <h3 className="text-center"> Liste des Questionnaires </h3>
             <div className="row">
  <div className="col-sm-5 col-md-8">
        
         <div className="questionnaire col-lg-4 col-md-4 col-sm-6 col-xs-6">
                    <a href="/categorie">
    <div className="thumbnail">
    <i className="fa fa-plus-circle"></i></div>
                    </a>
            </div>
        
        </div>
  <div className="col-sm-5 offset-sm-2 col-md-3 offset-md-0">
        <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
 
  <a className="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"> 
        <i className="fa fa-tint "></i> ADD Categorie <span class="badge pull-right">   </span>
        </a>
         <a className="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
        <i className="fa fa-th"></i> Questionnaires <span class="badge pull-right">  </span>
        
        </a>
  <a className="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">
        <i className="fa fa-cubes"></i> Blocs de questions <i class="fa fa-diamond premium"></i>
        </a>
  <a className="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">
        <i className="fa fa-folder"></i> Dossiers <i class="fa fa-diamond premium"></i>
        </a>
</div>

        
        </div>
</div>
          <div className="content">
        <div className="tab-content" id="v-pills-tabContent">
  <div className="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
        <h3 className="text-center"> <Qcms /></h3>
        
        
        </div>
  <div className="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
        <h3 className="text-center"> <Categories /> </h3>
        
        </div>
  <div className="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
        <h3 className="text-center"> Vos Blocs de Questions </h3>
        </div>
  <div className="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
        <h3 className="text-center"> <CreateCtg /> </h3>
        
        </div>
</div>
     </div>
        </div>
      </div>
    );
  }
}

export default Dashbord;
