import React, {Component} from 'react';
import {TabContent, TabPane, Nav, NavItem, NavLink, Progress, Label, Input} from 'reactstrap';
import classnames from 'classnames';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink
} from 'reactstrap';
import axios from 'axios';
import {
  getFromStorage,
  setInStorage,
} from '../../utils/storage';
import $ from 'jquery';
import DataTable from 'datatables.net';
class Aside extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1',
      usersession: [],
      categories: [],
      qcms:[],
    };
  }

  
  componentDidMount() {
    axios.get('/api/account/usersession')
    .then(res => {
      this.setState({ usersession: res.data });
   
    });
    axios.get('/api/account/categories')
    .then(res => {
      this.setState({ categories: res.data });
  
    }); 
  
    axios.get('/api/account/qcm')
    .then(res => {
      this.setState({ qcms: res.data });
    
    }); 
  }
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    $(document).ready( function () {
      setTimeout(function(){
        $('#ListSession').DataTable();
      }, 400);
    });
    return (
      <aside className="aside-menu">
        <Nav tabs>
       
          <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '1' })}
                     onClick={() => { this.toggle('1'); }}>
              <i className="icon-list"></i>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '2' })}
                     onClick={() => { this.toggle('2'); }}>
              <i className="icon-speech"></i>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '3' })}
                     onClick={() => { this.toggle('3'); }}>
              <i className="icon-settings"></i>
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
          <div className="callout m-0 py-2 text-muted text-center bg-light text-uppercase">
              <small><b>CATEGORIES</b></small>
            </div>
            

                  
                  {this.state.categories.map(categories => 
             
             <div> 
                <hr className="transparent mx-3 my-0"/>
            <div className="callout callout-danger m-0 py-3">
                New Categorie -<strong> {categories.titre}</strong> 
         
             <h6 className="text-muted mr-3">&nbsp; {categories.description}</h6>
              <small className="text-muted"><i className="icon-home"></i>&nbsp; By {categories.username}</small>
             </div>
             </div>
             
                )}
            <hr className="mx-3 my-0"/>
             
          
            <div className="callout callout-primary m-0 py-3">
             
            
              <div className="avatars-stack mt-2">
                <div className="avatar avatar-xs">
                  <img src={'img/avatars/2.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'img/avatars/3.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'img/avatars/4.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'img/avatars/5.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'img/avatars/6.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'img/avatars/7.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'img/avatars/8.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
              </div>
            </div>
            <hr className="transparent mx-3 my-0"/>
            
            <hr className="mx-3 my-0"/>
            <div className="callout m-0 py-2 text-muted text-center bg-light text-uppercase">
              <small><b>Members online</b></small>
            </div>
           
            <hr className="transparent mx-3 my-0"/>
            
            <div className="callout callout-warning m-0 py-3">
          
            <Table id="ListSession" responsive bordered>
            <thead>
              <tr>
                <th>User</th>
                <th>Date</th>
               
                </tr>
                </thead>
                <tbody>
              {this.state.usersession.map((usersession,i) => 
            
                
                      <tr>
                      <td> <div className="avatar float-center">
                 <img src={'img/avatars/7.jpg'} className="img-avatar"/>
                 </div>
                   <strong> {usersession.username}</strong>
                   <small className="text-muted">
                 <i className="icon-location-pin"></i>
                 &nbsp; {usersession.type},</small> </td>
                   <td> <div><small className="text-muted mr-3">
                 <i className="icon-calendar">
                 </i>&nbsp;{usersession.timestamp}</small> </div></td>
              
                </tr>
              
                 )}
              </tbody>
              </Table>
            </div>
            <hr className="mx-3 my-0"/>
             
          
          </TabPane>
          <TabPane tabId="2" className="p-3">
          {this.state.qcms.map(qcm =>
            <div className="message">
              <div className="py-3 pb-5 mr-3 float-left">
                <div className="avatar">
                  <img src={'https://cdn3.iconfinder.com/data/icons/brain-games/1042/Puzzle.png'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                  <span className="avatar-status badge-success"></span>
                </div>
              </div>
  
                <small className="text-muted">Created By {qcm.auteur}</small>
                <small className="text-muted float-right mt-1">{qcm.categorie}</small>
              
              <div className="text-truncate font-weight-bold"> {qcm.titre}</div>
              <small className="text-muted"> {qcm.description} </small>
             
            

            <hr/>

            </div> 
          )}
             
          </TabPane>
          <TabPane tabId="3" className="p-3">
            <h6>Settings</h6>

            <div className="aside-options">
              <div className="clearfix mt-4">
                <small><b>Option 1</b></small>
                <Label className="switch switch-text switch-pill switch-success switch-sm float-right">
                  <Input type="checkbox" className="switch-input" defaultChecked/>
                  <span className="switch-label" data-on="On" data-off="Off"></span>
                  <span className="switch-handle"></span>
                </Label>
              </div>
              <div>
                <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua.
                </small>
              </div>
            </div>

            <div className="aside-options">
              <div className="clearfix mt-3">
                <small><b>Option 2</b></small>
                <Label className="switch switch-text switch-pill switch-success switch-sm float-right">
                  <Input type="checkbox" className="switch-input"/>
                  <span className="switch-label" data-on="On" data-off="Off"></span>
                  <span className="switch-handle"></span>
                </Label>
              </div>
              <div>
                <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua.
                </small>
              </div>
            </div>

            <div className="aside-options">
              <div className="clearfix mt-3">
                <small><b>Option 3</b></small>
                <Label className="switch switch-text switch-pill switch-success switch-sm float-right">
                  <Input type="checkbox" className="switch-input"/>
                  <span className="switch-label" data-on="On" data-off="Off"></span>
                  <span className="switch-handle"></span>
                </Label>
              </div>
            </div>

            <div className="aside-options">
              <div className="clearfix mt-3">
                <small><b>Option 4</b></small>
                <Label className="switch switch-text switch-pill switch-success switch-sm float-right">
                  <Input type="checkbox" className="switch-input" defaultChecked/>
                  <span className="switch-label" data-on="On" data-off="Off"></span>
                  <span className="switch-handle"></span>
                </Label>
              </div>
            </div>

            <hr/>
            <h6>System Utilization</h6>

            <div className="text-uppercase mb-1 mt-4">
              <small><b>CPU Usage</b></small>
            </div>
            <Progress className="progress-xs" color="info" value="25"/>
            <small className="text-muted">348 Processes. 1/4 Cores.</small>

            <div className="text-uppercase mb-1 mt-2">
              <small><b>Memory Usage</b></small>
            </div>
            <Progress className="progress-xs" color="warning" value="70"/>
            <small className="text-muted">11444GB/16384MB</small>

            <div className="text-uppercase mb-1 mt-2">
              <small><b>SSD 1 Usage</b></small>
            </div>
            <Progress className="progress-xs" color="danger" value="95"/>
            <small className="text-muted">243GB/256GB</small>

            <div className="text-uppercase mb-1 mt-2">
              <small><b>SSD 2 Usage</b></small>
            </div>
            <Progress className="progress-xs" color="success" value="10"/>
            <small className="text-muted">25GB/256GB</small>
          </TabPane>
        </TabContent>
      </aside>
    )
  }
}

export default Aside;
